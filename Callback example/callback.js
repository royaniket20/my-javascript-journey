console.log('********Call Back example ************');

const func1 = function(a,b,callback){
    let c = a+b;
    callback(c);
}

const func2 = function(c){
    console.log('****Sub coming via Add ---- '+c);
}


func1(11,22,func2);

