/**
 * This is Book Class
 */


class Book{
   
   constructor(
    id,
    publisher,
    dateOfPublication,
    authors
   ){
       this.id=id,
       this.publisher = publisher,
       this.dateOfPublication=dateOfPublication
       this.authors=authors
   }

   addAuthors(data){
      // this.authors[this.authors.length] = data;
      this.authors.push(data);
   }
   updatePublisher(data){
       this.publisher = data;
   }


}

export default Book;