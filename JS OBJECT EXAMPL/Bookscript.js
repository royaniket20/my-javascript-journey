/**
 * Create a class for the Backpack object type.
 * @link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
 */
import Book from "./Book.js";

const myBook = new Book(
  111,
  'Asha Books 123',
  new Date(),
  []
);
myBook.addAuthors('sRIPARNA')
myBook.addAuthors('Aniket')
myBook.updatePublisher('new Publisher')
console.log("The Book :",myBook);


