//Old way Of creating Object
let object1 = {
  name: "aniket",
  isMarried: true,
  age: 28,
  toString: function () {
    console.log(
      `The Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
    );
  },
};

let object2 = {
  name: "Rimi",
  isMarried: true,
  age: 33,
  toString: function () {
    console.log(
      `The Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
    );
  },
};

object1.toString();
object2.toString();

//Create Object Using Constructor Function

function ObjectCreator(name, age) {
  this.name = name;
  this.age = age;
//   this.toString = function () {
//     console.log(
//       `The Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
//     );
//   }
//this.isMarried = false; --Prototyped
};
ObjectCreator.prototype.isMarried = false;
object1 = new ObjectCreator('Amit' , 333);
object2 = new ObjectCreator('Sasha' , 222);
object1.toString();
object2.toString();
//But here ---  this.isMarried = false; this.toString  is getting Duplicated on Each Object - Lets Prototype It 
ObjectCreator.prototype.isMarried = false;
ObjectCreator.prototype.toString = function () {
    console.log(
      `The Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
    );
  }
object1.toString();
object2.toString();
//Changing Prototype Value will affect all Objects
ObjectCreator.prototype.isMarried = 'NOT MARRIED';
ObjectCreator.prototype.toString = function () {
    console.log(
      `The UNKNOW NEW Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
    );
  }
object1.toString();
object2.toString();
//



