function ObjectCreatorOne(name, age) {
  this.name = name;
  this.age = age;
  //     this.toString = function () {
  //       console.log(
  //         `The Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
  //       );
  //     },
  //   this.isMarried = false;
}
ObjectCreatorOne.prototype.isMarried = false;
ObjectCreatorOne.prototype.toString = function (constructorName) {
  console.log(
    `The ${constructorName} Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
  );
};

function ObjectCreatorTwo(name, age) {
   this.name = name; 
   this.age = age;
  //     this.toString = function () {
  //       console.log(
  //         `The Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
  //       );
  //     },
  //   this.isMarried = true;
}
ObjectCreatorTwo.prototype = new ObjectCreatorOne(); //Chaiinng Done
ObjectCreatorTwo.prototype.isMarried = true; //Overriding Parent Value
// ObjectCreatorTwo.prototype.toString = function () { //This is Common Function as Chaiining is Done No need of this
//   console.log(
//     `The ObjectCreatorTwo Person : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
//   );
// };
let object1 = new ObjectCreatorOne("Amit", 333);
object1.toString('ObjectCreatorOne');
let object2 = new ObjectCreatorTwo("Ashoke", 222);
object2.toString('ObjectCreatorTwo');

//Update Value at Parent Will affect all chained Prototype Generated Objects[If Not Data Overridden]
ObjectCreatorOne.prototype.toString = function (constructorName) {
    console.log(
      `The ${constructorName} MODIFIED PERSON : ${this.name} is Married ? : ${this.isMarried} is of age : ${this.age}`
    );
  };
  object1.toString('ObjectCreatorOne');
  object2.toString('ObjectCreatorTwo');