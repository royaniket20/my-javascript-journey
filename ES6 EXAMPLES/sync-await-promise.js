//******** */
let browserfunction = function browserpromiseFunction(delay) {
    console.log("Now I am going to delay the Promise for Sec : " + delay);
    return new Promise((request, reject) => {
      setTimeout(request, delay * 1000);
    });
  };
  
  browserfunction(7).then(() => console.log("7 scond passed !!"));



//Promise in JS
let somePromiseWrapperFunction = function promiseFunction(delay) {
  console.log("Now I am going to delay the Promise for Sec : " + delay);
  return new Promise((request, reject) => {
   
    if (typeof delay !== "number") {
        throw new Error('My Error');
        console.log(i);
      reject(new Error("Some error happened"));
    } else {
      setTimeout(()=>{request("I am now all good")}, delay * 1000);
    }

  });
};

somePromiseWrapperFunction(5).then(() => console.log("No  second passed !!"));

let requestCaptur = (request) => {
  console.log(`The request captured is ${request}`);
};
let rejectCaptur = (reject) => {
  console.log(`The reject captured is ${reject}`);
};

let nextAction1 = () => {
  console.log("Now I am Next Action 1 !!!");
};
let nextAction2 = () => {
  console.log("Now I am Next Action 2 !!!");
};

//Subsequest functions being performed
somePromiseWrapperFunction("THREE")
  .then(requestCaptur).catch(rejectCaptur)
  // .then(requestCaptur, rejectCaptur) == Same thing as abiove 
  .then(nextAction1)
  .then(nextAction2);
somePromiseWrapperFunction(3)
  .then(requestCaptur, rejectCaptur)
  .then(nextAction1)
  .then(nextAction2);

console.log("Althoug I am finshed !!!");
