console.log("*****FOR CLASS EXAMPLES ES6 ******");

//Enhancing Object literals 

//old way 

let functionold = function(name , roll){

    return {name : name ,  // imporvied in es6
        roll : roll,
        printDetails : function(time){
            console.log(`The full details is ${name} -**- ${roll} -##- ${time} --`);
            return 'Aniket'; // Just for testing
        }
        };
}

console.log(`The dewtails are : ${JSON.stringify(functionold('aniket', 33))} . `);
console.log(functionold('rimi',22).printDetails(new Date()))


//Same thing can be done with less duplicate 

let functionNew = function(name , roll){

    return {name ,  // imporvied in es6
        roll ,
        printDetails : function(time){
            console.log(`The full details is ${name} -**- ${roll} -##- ${time} --`);
            return 'Aniket'; // Just for testing
        }
        };
}

console.log(`The dewtails are : ${JSON.stringify(functionNew('aniket', 33))} . `);
console.log(functionNew('rimi',22).printDetails(new Date()))


//********Creating Object with Spreads  */

let obj1 = {
    param1 : 'data1',
    param2 : 33,
    param3 : true
}
let objparam1 =  {
    param11 : 'data11',
    param22 : 111,
    param33 : false

}
let objparam2 =  {
    param1 : 'data22',
    param2 : 222,
    param3 : false

}
let objparam3 =  {
    param1 : 'data33',
    param2 : 333,
    param3 : false

}

let nestedObj  = {
    ...obj1 , 
    ...objparam1,
    ...objparam2,
    ...objparam3
}
console.log(nestedObj)
/**{
    param1: 'data33',
    param2: 333,
    param3: false,
    param11: 'data11',
    param22: 111,
    param33: false
  }- This is because the same 
attributes are used so it is replaced **/

//*We can use the Destructuring using function param also 

let strudentObj  = {
    name : 'aniket',
    title : 'mr' ,  // Only this is need
    subs : ['sci' , 'math'/**Only this needed */],
    address : {
        street : 'noone', //Onlyy this is needed
        pin : 711303
    }
}
console.log(strudentObj)
//Lets do destructuring - Noram lWay vs Function param way 

let {title , address , subs }  = {
    name : 'aniket',
    title : 'mr' ,  // Only this is need
    subs : ['sci' , 'math'/**Only this needed */],
    address : [{
        street : 'noone', //Onlyy this is needed
        pin : 711303
    },
    {
        street : 'noone22', //Onlyy this is needed
        pin : 711222
    }]
}


let destructuring = function ({title , address , subs }){
    console.log(`The details are : ${title} ---- ${address.street }  ---- ${subs}`)

}

destructuring(strudentObj)


//*************************** */
//for of can be used 

let courses  = new Map();
for (let count = 0; count < 10; count++) 
{
    courses.set("key"+count ,"monday"+count );
}

for (key of courses.keys()){
    console.log(key);
    /**
     * key0
key1
key2
key3
key4
key5
key6
key7
key8
key9
     */
}

for (val of courses.values()){
    console.log(val);
    /**
     * monday0
monday1
monday2
monday3
monday4
monday5
monday6
monday7
monday8
monday9
     */
}

for (entry of courses.entries()){
    console.log(`The key is : ${entry[0]} and Value : ${entry[1]}`);
}
for (letter of 'Aniket is a good guy'){
    console.log(letter);
}
/**
 * A
n
i
k
e
t
 
i
s
 
a
 
g
o
o
d
 
g
u
y
 */









