const spacePeople = () => {
  return new Promise((resolve, reject) => {
    let api = "http://api.open-notify.org/astros.json";
    //Browser provide This by default - this is Node js use case
    let XMLHttpRequest = require("xhr2");
    let request = new XMLHttpRequest();
    request.open("GET", api);
    //When Load successful that callback
    request.onload = () => {
      if (request.status === 200) {
        resolve(JSON.parse(request.response));
      } else {
        reject(Error(request.status + "*******"));
      }
    };
    //when Nwk issue or some such happens
    request.onerror = (err) => {
      // return it to reject Object
      reject(err);
    };
    //Fire the request
    request.send();
  });
};

let promisereference = spacePeople();

promisereference.then((responseFromPromise) => {
  console.log(responseFromPromise),
    (rejectsFromPromise) => {
      console.log(rejectsFromPromise);
    };
});

//Newer way using Fetch


const spacePeople2 = () => {
  //Node JS use case : Browser provide fetch by default
    const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
  let api = "http://api.open-notify.org/astros.json";
  return fetch(api).then((resp) => resp.json());
};

spacePeople2()
.then(resp=> resp.people) //Return peoples array 
.then(peoples=> peoples.map(p=>p.name)) // return Nasmes array
.then(names=> names.join(",\n")) // String - comma seperated array Str
.then(console.log);
console.log('*****I am already Done !!!!');

//Async /Await 


const gitHubRequest = async (user ) => {

    //Node JS use case : Browser provide fetch by default
    const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
  
  let responseMap = new Map();

  let response1 = await fetch (`https://api.github.com/users/${user}`).then(response=>response.json());

  let response2 = await  fetch("http://api.open-notify.org/astros.json").then((resp) => resp.json());

await promisereference.then(responseFromPromise =>  responseMap.set('AUSTRONAUT-PROMISE-RESPONSE',responseFromPromise));
  responseMap.set('GITHUB-RESPONSE',response1);
  responseMap.set('ASTRONAUT-REPOSNE',response2);
 console.log(responseMap);

}

gitHubRequest('codeForCoders');
