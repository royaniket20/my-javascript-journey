const id = Symbol();

const studentObj = {
  name: "aniket",
  subjects: ["sub1", "sub2"],
  id: "school-id", //No conflict will happen
};
studentObj[id] = 1111;

console.log(studentObj);

console.log(studentObj[id]); //accessing Symbol value

console.log(studentObj.id); // accessing the defined vale

/**
 * {
  name: 'aniket',
  subjects: [ 'sub1', 'sub2' ],
  id: 'school-id',
  [Symbol()]: 1111
}
1111
school-id
 */

//Working with Maps 

let courses  = new Map();
courses.set("key1" ,"monday" );
courses.set(new Date(), {name : "aniket" , roll : 22});
   

console.log('Courses are ' ,courses ,'Size is ' , courses.size)

console.log(courses.get('key1'))
const callbackFunction = (item) =>{
    console.log('Iteration is ' , item)
}
courses.forEach(callbackFunction)

//Working with Sets 

let books = new Set();
books.add("Pride and Prejudice");
books.add("War and Peace").add("Oliver Twist");
books.add("Pride and Prejudice");

console.log(books);
console.log(books.size);

books.delete("Oliver Twist");

console.log(
  "has Oliver Twist",
  books.has("Oliver Twist")
);


//Spread operator in array  ...

let arr1 = [1,2,3,4];
let arr2 = [2,3,4,5];

let finalArr = [11,22,33,arr1,arr2,44,55,66];

console.log(finalArr);
//[ 11, 22, 33, [ 1, 2, 3, 4 ], [ 2, 3, 4, 5 ], 44, 55, 66 ]

finalArr =  [11,22,33,...arr1,...arr2,44,55,66];
console.log(finalArr);
/**
 * [
  11, 22, 33, 1, 2,  3,
   4,  2,  3, 4, 5, 44,
  55, 66
]
 */

//Destructuring the Array 
let arr3 = [11,22,33,44];

let [first , second , last] = [11,22,33,44]

console.log(arr3[0] , 'Same as ', first)
//11 Same as  11

console.log('The Value present ? ', arr3.includes(44))
