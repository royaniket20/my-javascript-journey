class Vehicle {
  color;
  description;
  wheels;
  constructor(color, description) {
    this.color = color;
    this.description = description;
  }

  get getColor() {
    return this.color;
  }

  set setWheels(numOfWheels) {
    this.wheels = numOfWheels;
  }

  toString() {
    console.log(
      `--T OSTRING IMPL : ${this.color} ---- ${this.description} --- ${this.wheels}`
    );
  }
}

class FourWheeler extends Vehicle {
  manufacturer;
  bonetsize;
  wipercount;
  constructor(manufacturer, bonetsize, wipercount, color, description) {
    super(color, description);
    this.bonetsize = 100 * -bonetsize;
    this.manufacturer = manufacturer;
    this.wipercount = wipercount;
  }

  get getColor() {
    return super.getColor; //'Overriddent Method';
  }

  toString() {
    super.toString();
    console.log(
      `--T OSTRING IMPL : ${this.manufacturer} ---- ${this.bonetsize} --- ${this.wipercount}`
    );
  }
}

let myCar = new FourWheeler("Maruti", 20, 4, "Red", "MyFirst car");
console.log(myCar);

myCar.toString();
/**
 * --T OSTRING IMPL : Red ---- MyFirst car --- undefined
--T OSTRING IMPL : Maruti ---- -2000 --- 4
 */

myCar.setWheels = 88;

myCar.toString();
/**
 * --T OSTRING IMPL : Red ---- MyFirst car --- 88
--T OSTRING IMPL : Maruti ---- -2000 --- 4

 */

console.log(`Getter value example : ${myCar.getColor}`);
//Getter value example : Red //  Getter value example : Overriddent Method
