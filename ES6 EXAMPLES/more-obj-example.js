
//Some more Object example 

let someObj = {
    name : 'aniket',
    rolle : 22 , 
    dob : new Date("October 13, 2014 11:13:00"),
    printmessage(){
       return (`The user is : ${this.dob}`);
    },
    sndreference(){
        let somefunction = function printdata(){
            console.log('I will print some data '+ this.name);
        }
        return somefunction;
    },

    sndreferenceArrow(){
        let somefunction = ()=>{
            console.log('I will print some data Arrow :  '+ this.name);
        }
        return somefunction;
    }
}

console.log(`The Object is : ${someObj.printmessage()}`);
let innerFunc = someObj.sndreference();
console.log(`The Object is : ${innerFunc()}`);
//I will print some data undefined
name = 'Global Name'
console.log(`The Object is : ${innerFunc()}`);
//I will print some data Global Name
 innerFunc = someObj.sndreferenceArrow();
console.log(`The Object is : ${innerFunc()}`);
