console.log("****All function stuffs*****");

let parentData = "Some Outer Data";

let someData = {
  parentData: "Aniket Roy",
  //POSR ES6 STYLE
  mydataWithOutArrowFunction: function () {
    console.log(`----My data is : ${this.parentData} `);
  },

  //DENGAROUS - DO NOT DO IT
  mydataWithArrowFunction: () => {
    console.log(`----My data is : ${this.parentData} `);
  },

  //PRE ES6 Style
  myDataDirect() {
    console.log(`----My data is : ${this.parentData} `);
  },
};

someData.mydataWithOutArrowFunction();
//----My data is : Aniket Roy
someData.mydataWithArrowFunction();
//----My data is : undefined  - This is happening because Arraw function cannot hold
//the immediate enclosing reference
someData.myDataDirect();
//----My data is : Aniket Roy

//***************Default values  */

let somefunction = function (a = 4, b = 4) {
  return a + b;
};

console.log(`--Total sum is ${somefunction(11, 22)}`);
console.log(`--Total sum is ${somefunction(22)}`); //default kick in

//*************user of String repet Function */

let str = "Hot Weather !";
console.log(`${str.repeat(2)}`);

//**********evaluation of allow function******* */

somefunction = (a = 4, b = 4) => {
  return a + b;
};
console.log(`--Total sum is ${somefunction()}`); //default kick in

somefunction = (a = 4, b = 4) => a + b;

console.log(`--Total sum is ${somefunction()}`); //default kick in

//**************This and arrow Function  -- This is importat for arrow  */

/**
 * This always Get scope of the parent of immediate Parent
 */

let student = {
  name: "Aniket",
  age: 22,
  subjects: ["Bengali", "English", "Math"],
  printhobbies() {
    //This is a Object method here this is bound to the Object
    console.log(`The subjects are : ${this.subjects}`); //Here this is Bound to student
    this.subjects.forEach(function (subject) {
      //these are callback so they are nothing but just like inmdependent function
      console.log(
        `Indivisual Subject is : ${subject} and it is read by ${this.name}`
      );
      //Indivisual Subject is : Math and it is read by undefined == Here 'this' is Not working
      //Indivisual Subject is : Math and it is read by global name
    });

    //Bypassing the global Scope
    let that = this; //Now this is captured
    this.subjects.forEach(function (subject) {
      console.log(
        `Indivisual Subject is : ${subject} and it is read by ${that.name}`
      );
      //Indivisual Subject is : Math and it is written by Aniket == Here 'this' is  working via 'that'
    });

    //A better way here
    this.subjects.forEach((subject) => {
      //THE MOMENT you use Arraw Function they start using the lexical enclosing scope that is the
      console.log(
        `Indivisual Subject is : ${subject} and it is written by ${this.name}`
      );
      //Indivisual Subject is : Math and it is written by Aniket == Here 'this' is  working
    });
  },
};
name = "global name";

student.printhobbies();

//
let normalFunction = function addval() {
  console.log(this.nameData); //This will resolve from Caller
};

let callerFunc = function caller() {
  nameData = "aniket"; //this will use this
  normalFunction();
};

callerFunc();

//***************Using yield and generator */

let generatorFunction = function* testing(){
    console.log('I am at first 1 step');
    yield 'first1' ;
    console.log('I am at first 2 step');
    yield('first2');
    console.log('I am at first 3 step');
    yield('first3');
    console.log('I am at first 4 step - Last step');
}

let counter = generatorFunction();

console.log(counter.next());
console.log(counter.next());
console.log(counter.next());
console.log(counter.next());

/**
 * I am at first 1 step
{ value: 'first1', done: false }
I am at first 2 step
{ value: 'first2', done: false }
I am at first 3 step
{ value: 'first3', done: false }
I am at first 4 step - Last step
{ value: undefined, done: true }
 */


