let arr = ["aa", "bb", "cc"];
//If array is already flastten No Flattening will hapen
let arr1 = arr.flatMap((item) => item.split(""));

console.log(arr1);
//[ 'a', 'a', 'b', 'b', 'c', 'c' ]
//REMEMEBER FIRSRT IT MAP AND THEN FLAT

arr = [["aa"], ["bb"], ["cc"]];
//If array is already flastten No Flattening will hapen
arr1 = arr.flatMap((item) => item[0].split(""));

console.log(arr1);
//[ 'a', 'a', 'b', 'b', 'c', 'c' ]

arr = [[20], [30], [40]];
//ALL BELOW ARE SAME
arr1 = arr.flatMap((item) => item * 2);
arr1 = arr.flatMap((item) => item[0] * 2);

console.log(arr1);
//[ 40, 60, 80 ]
arr = [
  [
    { name: "aniket", roll: 23 },
    { name: "amit", roll: 23 },
  ],
  ,
  [{ name: "ashoke", roll: 23 }],
  [{ name: "anusha", roll: 23, addres: { street: "Kolkata", pin: 711303 } }],
];

//If array is partially Nested then also It will do One level Flattening
arr1 = arr.flatMap((item) => {
  console.log(`The Current Item - ${JSON.stringify(item)}`);
  item.map((data) => (data.age = 33));
  console.log(`The Updated Item - ${JSON.stringify(item)}}`);
  return item;
});

console.log(arr1);
