console.log("---------------");

const function2 = function localScope(base) {
  let rate = 0.5; // this will be garbage collected as soon as function ends
  let result = base * rate;
  rate++;
  console.log("updated rate is : " + rate);
  return result; // this is call by value
};

const function1 = function outerscope() {
  let rate = 0.5; // this will not be garbage collected
  //because it has still acctive reference even after the function ends

  let reference = function innerscope(base) {
    let result = base * rate;
    rate++;
    return result; // this is call by value
  };
  return reference; //call by reference
};

console.log(`The result is = ${function2(55)}`);

//Because everytime the rate is getting garbage collected so no meaning of ++ operation
console.log(`The result is = ${function2(55)}`);

console.log("****************************************");
let outercaller1 = function1();
let outercaller2 = function1(); // different closure

console.log(`The result is = ${outercaller1(55)}`);

//Because  the rate is not getting garbage collected due to internal reference
console.log(`The result is = ${outercaller1(55)}`);

console.log(`The result is = ${outercaller2(55)}`);
