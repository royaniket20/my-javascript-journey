console.log('-********-');

let item = 'item1';

const arr = [1,2,true , null , 4.6 , {name : 'aniket' , rolle : 12},item];

console.log(arr);

//We can use any index Point 

arr[100] = 'aniket'

console.log(arr);

console.log(arr.join(' , '));

arr.push('Aniket 2')
arr.unshift('first item')

console.log(arr);


//We can pop out one element from arr 

console.log(arr.pop()+'------'+arr.shift());
console.log(arr);

//Unshift add lelment at first position 
//shift remve element from firt position 
//pop remove element from last 
//push add element at last point 
//foreach will iterte over array 

arr.forEach(function(data){
    //We do not touch arr - But use it 
    console.log('-----'+JSON.stringify(data));
})

//find -- this function same as java Stream Filter 

arr.find(function(data){

   if( typeof data == Object){
     console.log('---****---' + typeof data);
   }
})