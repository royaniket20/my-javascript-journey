
var intervalId;
let generateRandomIndexFruit = function ( arr : any){
    let index = Math.floor(Math.random() * arr.length);
    return arr[index];
}
let fruiEmojis = ['🍏' ,'🍎', '🍐', '🍊', '🍋', '🍌', '🍉', '🍇', '🍓', '🍈', '🍒', '🍑', '🥭']
 function countDown(initial : number , finishCallback:CallableFunction,final=0 ,interval = 1   ){
   let uiElement = document.getElementById('food-display'); 
  
   uiElement.innerHTML =generateRandomIndexFruit(fruiEmojis);
   initial--;
      intervalId = setInterval(function(){
        uiElement.innerHTML =generateRandomIndexFruit(fruiEmojis);
        console.log('Value Printed : '+initial);
        initial--;
        if(initial<0)
        {
            clearInterval(intervalId);
            finishCallback('done')
        }
      }, interval*1000); 

}
 function onClickStartAction(){
    let printer = (data: any)=>{
        console.log(data);
        let uiElement3 = document.getElementById('food-display');
        let uiElement = document.getElementById('start-button-section');
        let uiElement2 = document.getElementById('stop-button');
        uiElement3.innerHTML = 'Wake me Up 😴'
       uiElement.hidden = false;
        uiElement2.hidden = true;
        
    }
     let inputValue  = parseInt( (document.getElementById('timerValInput') as HTMLInputElement).value); 
     console.log(inputValue);
     
    if(isNaN(inputValue) )
    {
        let uiElement = document.getElementById('invalid-input-1');
        if(uiElement.hidden)
        {
        uiElement.hidden = false;
        setTimeout(function(){
        uiElement.hidden = true;
        },2000);}
    }else if(!isNaN(inputValue) && fruiEmojis.length<inputValue )
    {
        let uiElement = document.getElementById('invalid-input-2');
        if(uiElement.hidden)
        {
        uiElement.hidden = false;
        setTimeout(function(){
        uiElement.hidden = true;
        },2000);}
    }else{
     countDown(inputValue ,printer);
    let uiElement = document.getElementById('start-button-section');
     let uiElement2 = document.getElementById('stop-button');
    uiElement.hidden = true;
    uiElement2.hidden = false;
    }
    
 } 


function onClickStopAction(){
    
    clearInterval(intervalId);
    let uiElement = document.getElementById('start-button-section');
     let uiElement2 = document.getElementById('stop-button');
     let uiElement3 = document.getElementById('food-display');
     uiElement3.innerHTML = 'Wake me Up 😴'
    uiElement.hidden = false;
     uiElement2.hidden = true;
   
} 

