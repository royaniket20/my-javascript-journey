/**
 * ECMAScript is the Standarization of JS language specification 
 * to have interoperability in WWW 
 * ECMSScript is Specicifcation that tells How Bowser shoud Implement It 
 * V8 Engine - Most popular JS Engine /Interpreter  - Every browser have their own Engine 
 * V8 - Made by Google -->Used in node Js 
 * **/ 

/**
 * NodeJS is used for running Server Side programming using Javascript 
 */

/**
 * JavaScript also Known as Live Script - Majority of Web Browser support It 
 * It is Not Java  :)
 */

/**
 * JavaScript is Inteprpreted Language and No compilation required - Dynamically typed language 
 * 
 */

// Data Types
var firstName = "Jamila";
var age = 21;
var isFemale = true;
var              balance = 100.32;
var dob = new Date(2000, 1, 28);
var person = {};
var empty = undefined;

console.log(typeof firstName);
    console.log(typeof age);
console.log(typeof isFemale);
console.log(typeof balance);
console.log(typeof dob);
console.log(typeof person);
console.log(typeof empty);

//String --Commit test

var brand = 'aniket'
console.log(brand);
console.log(brand.length + '****' + brand.toUpperCase() + '-----' + brand.length)

console.log(`${brand}  ${brand.toUpperCase()}`)

var student = {
    name: 'aniket',
    roll: 12,
    age: 3,
    subjects: ['bengali', 'english'],
    address: {
        city: 'bagnan',
        pins: [1, 2, 3, 4]
    }
};

console.log('*/*******' + JSON.stringify(student));
console.log(student.address.pins[2]);

console.log(Object.keys(student));
console.log(Object.values(student));

//Boolean 
var isAdult = !true;
console.log(isAdult);

if (isAdult) {
    console.log('********');
} else {
    console.log('#######');
}

var a = 5 + 6;
var a = 5 + '' + 6;
var exponentiation = 4 ** 4;
console.log(exponentiation);

function add(a, b) {
    return a + b;
}

console.log(add(33.6, 7));

for (let index = 0; index <10; index++) {
   console.log(index);
    
}

for(const pin of student.address.pins)
{
    console.log(pin + '*******');
}

student.address.pins.forEach(function(item){
    console.log(item + '-----');
} )

var dataNum = 5;
console.log(dataNum++);
console.log(++dataNum);

//Hoisting cause Problem - var is present even Outside the the Scope alse

//Because the Interpreter lookas at all the variable available in system and use the same One - It do not 
//create for scope specific

for (var index1 = 0; index1 < 10; index1++) {
   console.log(index1+'&*&*&*&');
    
}
//This will still Log value  even outside Scope
console.log(index1);

//Better we shoud use let 

for (let index2 = 0; index2 < 10; index2++) {
    console.log(index2+'&*&*&*&');
     
 }
 //This will Not  Log value  outside Scope
 //console.log(index2); //--Undefined 


//let vs  const 

//Let variable type can be changed 
//use const then to make type locked once assigned val  - No reassignment is allowed anymore  - Object ,array Manipulation is allowd as is 

let brandData = 'aniket'
 brandData  = true
 brandData = function(){
    return 'Data from function'
} 
console.log(brandData());

const brandData2 = 'aniket'
// brandData2  = true
// brandData2 = function(){
//     return 'Data from function'
// } 
console.log(brandData2);

const brandData3 = {};

console.log(brandData3);

//Object manippulation can be done 
//We are using bracket convenstion
brandData3["name"] = "data data"
//Using some variable 
var someReservedKeyword = "true";
brandData3[someReservedKeyword] = "We conquer Reserved Space !! "
//we are using . notation
brandData3.bame = 'Aniket Roy'
brandData3["age"] = 100

console.log(brandData3); //{ name: 'data data', age: 100 }

delete brandData3["age"]

console.log(brandData3); //{ name: 'data data' }

//Var shoud never be used anymore 

//Object having method example 

const human = {
    name: 'aniket',
    roll: 12,
    age: 3,
    subjects: ['bengali', 'english'],
    address: {
        city: 'bagnan',
        pins: [1, 2, 3, 4]
    },

    toggleCity : function(currentCity){
        this.address.city = currentCity
    }
};

console.log(JSON.stringify(human));

human.toggleCity('kolkata')

console.log(JSON.stringify(human.address));






