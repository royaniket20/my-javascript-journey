//import Course from './Course.js'
//Course - default export can be changed to any name
import CurrentCourse, {
  printCourse,
  printSomethingElse as printSomething,
  printMyName,
} from "./Course.js";
//Here we have used alias during Import for printSomethingElse -
//But we must refer to exact name it exported or the alias it exported

let study = new CurrentCourse("100", "Computer Scient", 3);

let pElement = document.querySelector("p");
printMyName();
printCourse();
pElement.innerHTML = study.getNmeOfCourse();
pElement.innerHTML += printSomething(11, 22);
