//DEFAULT KEYWORK CAN be used only once in a file
export default class Course {
  id;
  name;
  durationInMoths;
  constructor(id, name, duration) {
    this.id = id;
    this.name = name;
    this.durationInMoths = duration;
  }

  getNmeOfCourse() {
    return this.name;
  }
}

//inline Export
export function printSomethingElse(num1, num2) {
  console.log("The Sum is - " + (num1 + num2));
  return num1 + num2;
}

function printCourse(course) {
  console.log(`The Course Details  ${course}`);
}

function dummyPrint() {
  console.log(`I am Aniket Roy`);
}

//export default Course; - Replaced with Inline Export default

export { printCourse, dummyPrint as printMyName }; //named export with one using alias
